package factory

import akka.actor.ActorSystem
import global.{IdCollection, MapCollection}
import model.game.{GameMap, Grid, Player}
import model.online.User
import model.unit.Building

import scala.collection.mutable

/**
  * Created by Trung on 4/2/2018.
  */
object MapFactory {

    /**
      * Create new player instance
      * @return
      */
    def createMap(system: ActorSystem) = {
        val id: Int = IdCollection.mapId
        val gameMap = system.actorOf(GameMap.props(id), s"map_$id")
        gameMap
    }
}
