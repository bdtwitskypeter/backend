package factory

import akka.pattern.ask
import akka.actor.ActorRef
import akka.util.Timeout
import global.{IdCollection, MapCollection}
import model.game.{Grid, Player}
import model.unit.Building
import service.impl.SearchServiceImpl

import scala.collection.mutable
import scala.concurrent.{Await, Future}
import scala.concurrent.duration._

/**
  * Created by Trung on 4/3/2018.
  */

object BuildingFactory {

  implicit val timeout: Timeout = 5 seconds
  /**
    * Make character by player and position
    *
    * @param player
    * @param posX
    * @param posY
    */
  def makeBuilding(player: ActorRef, map: mutable.MutableList[Grid], buildings: mutable.MutableList[Building], posX: Int, posY: Int): Option[Building] = {
    val g = SearchServiceImpl.searchGridByPos(map, posX, posY)
    val grid = g match {
      case Some(d) ⇒ d
      case None ⇒ new Grid
    }

    val playerAPFut: Future[Int] = (player ? Player.GetActionPoint).mapTo[Int]
    val playerAP: Int = Await.result(playerAPFut, 3 seconds)

    if (grid.gameEntity == null && playerAP >= 2) {
      var building = new Building

      // Set gameEntity parameters
      building.id = IdCollection.gameEntityId
      building.requiredActionPoint = 2
      building.currentHP = 100
      building.maxHP = 100
      building.visionRange = 2

      val playerIdFut: Future[Int] = (player ? Player.GetId).mapTo[Int]
      val playerId: Int = Await.result(playerIdFut, 3 seconds)
      building.owner = playerId

      // Set character parameters
      building._isAttacked = false

      player ! Player.UseAp
      player ! Player.UseAp

      // Update map collection
      buildings += building

      grid.gameEntity = building
      Some(building)
    }
    None
  }
}
