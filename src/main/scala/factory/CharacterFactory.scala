package factory

import akka.pattern.ask
import akka.actor.ActorRef
import akka.util.Timeout
import global.IdCollection
import model.game.{Grid, Player}
import model.unit.Character
import service.impl.SearchServiceImpl

import scala.collection.mutable
import scala.concurrent.{Await, Future}
import scala.concurrent.duration._

/**
  * Created by Trung on 4/3/2018.
  */

object CharacterFactory {

  implicit val timeout: Timeout = 5 seconds
  /**
    * Make character by player and position
    *
    * @param player
    * @param posX
    * @param posY
    */
  def makeCharacter(player: ActorRef, map: mutable.MutableList[Grid], map_id: Int, posX: Int, posY: Int): Unit = {

    val g = SearchServiceImpl.searchGridByPos(map, posX, posY)
    val grid = g match {
      case Some(d) ⇒ d
      case None ⇒ new Grid
    }


    val playerAPFut: Future[Int] = (player ? Player.GetActionPoint).mapTo[Int]
    val playerAP: Int = Await.result(playerAPFut, 3 seconds)


    if (grid.gameEntity == null && playerAP >= 2) {
      var character = new Character

      // Set gameEntity parameters
      character.id = IdCollection.gameEntityId
      character.requiredActionPoint = 2
      character.currentHP = 100
      character.maxHP = 100
      character.visionRange = 2
      val playerIdFut: Future[Int] = (player ? Player.GetId).mapTo[Int]
      val playerId: Int = Await.result(playerIdFut, 3 seconds)
      character.owner = playerId
      character.mapId = map_id

      // Set character parameters
      character._isAttacked = false
      character.attacking = false

      player ! Player.UseAp
      player ! Player.UseAp
      grid.gameEntity = character
      character
    }
  }
}
