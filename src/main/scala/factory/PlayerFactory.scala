package factory

import akka.actor.ActorSystem
import global.IdCollection
import model.game.Player
import model.online.User

/**
  * Created by Trung on 4/2/2018.
  */
object PlayerFactory {

    /**
      * Create new player instance
      * @return
      */
    def createPlayer(system: ActorSystem, user: User, mapId: Int) = {
        val player = system.actorOf(Player.props(user, mapId))
        player
    }
}
