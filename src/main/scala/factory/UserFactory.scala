package factory

import global.{IdCollection, UserCollection}
import model.online.User

/**
  * Created by Trung on 4/4/2018.
  */
object UserFactory {

    /**
      * Create user
      * @param name
      * @return
      */
    def createUser(name: String) = {

        var user = new User

        user.id = IdCollection.userId
        user.name = name
        user.roomId = 0
        user.inGame = false
        user.inRoom = false

        UserCollection.users += user.id -> user
        user
    }
}
