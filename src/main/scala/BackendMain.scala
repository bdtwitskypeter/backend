import service.impl.HandlerService
import scala.concurrent.ExecutionContext.Implicits._

import scala.io.StdIn



object BackendMain {

  val SERVER_HOST = "localhost"
  val SERVER_PORT = 8080

  def main(args: Array[String]): Unit = {

    val service = new HandlerService(SERVER_HOST, SERVER_PORT)



    println(s"Server running at http://$SERVER_HOST:$SERVER_PORT")
    println("Press <return> to stop")
    StdIn.readLine()

    service.bindingFuture
      .flatMap(_.unbind())
      .onComplete{ _ ⇒ service.system.terminate() }
  }

}


