package service

import akka.actor.ActorRef
import model.game.{GameMap, Grid, Player}
import model.unit.Building

import scala.collection.mutable
/**
  * Created by Trung on 4/2/2018.
  */
trait GameplayService {

    /**
      * Assign one entity to attack
      * @param map
      * @param src
      * @param dest
      */
    def move(map: mutable.MutableList[Grid], src: Int Tuple2 Int, dest: Int Tuple2 Int)

    /**
      * Build tower
      * @param player
      * @param building
      * @param map
      * @param dest
      */
    def build(player: ActorRef, building: Building, buildings: mutable.MutableList[Building], map: mutable.MutableList[Grid], dest: Int Tuple2 Int)
}
