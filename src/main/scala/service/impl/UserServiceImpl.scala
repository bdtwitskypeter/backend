package service.impl

import global.UserCollection
import model.online.User
import service.UserService

/**
  * Created by Trung on 4/4/2018.
  */
object UserServiceImpl extends UserService{

    /**
      * User join room
      *
      * @param roomId
      */
    override def joinRoom(user: User, roomId: Int): Unit = {
        user.inRoom = true
        user.roomId = roomId
    }

    /**
      * Find user object by id
      *
      * @param userId
      */
    override def findUserById(userId: Int): Option[User] = {
        val keySet = UserCollection.users.keySet

        if (keySet.contains(userId))
            UserCollection.users.get(userId)
        null
    }
}
