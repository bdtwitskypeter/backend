package service.impl

import model.game.GameMap
import model.game.Grid
import model.unit.Building
import service.TerritoryService

import scala.collection.mutable

/**
  * Created by Trung on 3/22/2018.
  */
object TerritoryServiceImpl extends TerritoryService{

    /**
      * Update territory
      * @param map
      */
    override def updateTerritory(map: GameMap): Unit = {
        val buildingList = map.buildingList
        val gridList = map.gridList
        buildingList.sortBy(_.timeStamp) // Sort by build time

        resetGridOwner(map) // Reset map

        for (building <- buildingList) setTerritoryOwner(building, gridList)
    }

    /**
      * Set territory owner by building
      * @param building
      * @param gridList
      */
    private def setTerritoryOwner(building: Building, gridList: mutable.MutableList[Grid]): Unit = {
        val VISION_RANGE = building.visionRange

        for (grid <- gridList) {

            val gridX = grid.positionX
            val gridY = grid.positionY
            val distance = Math.abs(gridX - grid.positionX) + Math.abs(gridY - grid.positionY)

            if (distance <= VISION_RANGE && grid.owner == null)
                grid.owner = building.owner
        }
    }

    /**
      * Revert all grid owners to null
      * @param map
      */
    private def resetGridOwner(map: GameMap): Unit = {
        val gridList = map.gridList

        for (grid <- gridList) grid.owner = 0
    }
}
