package service.impl

import akka.pattern.ask
import akka.actor.{ActorRef, ActorSystem}
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.{Directives, Route}
import akka.stream.ActorMaterializer
import akka.util.Timeout
import factory.UserFactory
import global.UserCollection
import model.api.Room
import model.game.Player
import model.online.User
import service._

import scala.concurrent.duration._
import scala.concurrent.{Await, ExecutionContextExecutor, Future}

class HandlerService(server_host: String, server_port: Int) extends Directives with JsonSupport {
  implicit val system: ActorSystem = ActorSystem("backend")
  implicit val materializer: ActorMaterializer = ActorMaterializer()
  implicit val executionContext: ExecutionContextExecutor = system.dispatcher
  implicit val timeout: Timeout = 5 seconds

  def userCreate = post {
    entity(as[UserCreation]) { create ⇒
      val user_name = create.user_name
      val user = UserFactory.createUser(user_name)
      complete(200, user.id.toString)
    }
  }

  def roomCreate = post {
    entity(as[RoomCreation]) { create ⇒
      val player_id = create.player_id
      val user: Option[User] = UserCollection.users.get(player_id)
      user match {
        case Some(u) ⇒
          val room_id = Room.getID()
          val r: ActorRef = system.actorOf(Room.props(), room_id.toString)
          r ! Room.join(u)
          complete(room_id.toString)
        case None =>
          complete(400, "BAD")
      }
    }
  }

  def roomStart = post {
    entity(as[RoomStart]) { start =>
      val room_id = start.room_id
      system.actorSelection(s"user/${room_id.toString}") ! Room.start()
      complete("OK")
    }
  }

  def playerExit = post {
    entity(as[PlayerExit]) { exit =>
      val player_id = exit.player_id
      Player
      complete("OK")
    }
  }

  def playerJoin = post {
    entity(as[PlayerJoin]) { join =>
      val room_id = join.room_id
      val player_id = join.player_id
      UserCollection.users.get(player_id) match {
        case Some(u) ⇒ system.actorSelection(s"user/${room_id.toString}") ! Room.join(u)
          complete("OK")
        case None ⇒ complete(400, "BAD")
      }
    }
  }

  def actionMove = post {
    entity(as[MoveAction]) { move =>
      val game_id = move.game_id
      system.actorSelection(s"user/${game_id.toString}") ! Room.move(move)
      complete("OK")
    }
  }

  def actionBuild = post {
    entity(as[BuildAction]) { build =>
      val dest = (build.posX_dest, build.posY_dest)
      val game_id = build.game_id
      val player_id = build.player_id
      system.actorSelection(s"user/${game_id.toString}") ! Room.build(build)
      complete("OK")
    }
  }

  def gameUpdate = get {
    val game_id = headerValueByName("game_id")

    try {
      val a: Future[ActorRef] = system.actorSelection(s"/user/$game_id").resolveOne()
      val actor: ActorRef = Await.result(a, 5 seconds)
      val b: Future[Room.statusResponse] = (actor ? Room.status).mapTo[Room.statusResponse]
      val response: Room.statusResponse = Await.result(b, 5 seconds)
      complete {
        response.msg
      }
    } catch {
      case _: concurrent.TimeoutException ⇒ complete(400, "BAD")
      case _: akka.actor.ActorNotFound ⇒ complete(400, "BAD")
    }
    complete("OK")
  }

  val route: Route =
    pathPrefix("user") {
      path("create") {
        userCreate
      }
    } ~
      pathPrefix("room") {
        path("create") {
          roomCreate
        } ~
          path("start") {
            roomStart
          }
      } ~
      pathPrefix("player") {
        path("exit") {
          playerExit
        } ~
          path("join") {
            playerJoin
          }
      } ~
      pathPrefix("action") {
        path("move") {
          actionMove
        } ~
          path("build") {
            actionBuild
          }
      } ~
      pathPrefix("game") {
        path("update") {
          gameUpdate
        }
      }

  val bindingFuture: Future[Http.ServerBinding] =
    Http().bindAndHandle(route, server_host, server_port)
}