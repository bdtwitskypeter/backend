package service.impl

import global.MapCollection
import model.game.Grid
import model.game.GameMap
import service.SearchService

import scala.collection.mutable

/**
  * Created by Trung on 4/3/2018.
  */
object SearchServiceImpl extends SearchService{

    /**
      * Search for grid given grid
      * @param posX
      * @param posY
      */
    override def searchGridByPos(map: mutable.MutableList[Grid], posX: Int, posY: Int): Option[Grid] = {

        for (grid <- map)
            if (grid.positionX == posX && grid.positionY == posY)
                Some(grid)
        None
    }

    /**
      * Search map by map id
      *
      * @param id
      */
    override def searchMapById(id: Int): Option[GameMap] = MapCollection.map.get(id)
}
