package service.impl

import akka.pattern.ask
import akka.actor.ActorRef
import akka.util.Timeout
import model.game.{GameEntity, GameMap, Grid, Player}
import model.interface.{Attackable, Moveable, Targetable}
import model.unit.Building
import service.GameplayService

import scala.collection.mutable
import scala.concurrent.{Await, Future}
import scala.concurrent.duration._

/**
  * Created by Trung on 4/2/2018.
  */
object GameplayServiceImpl extends GameplayService {

  /**
    * Assign one entity to attack
    *
    * @param map
    * @param src
    * @param dest
    */
  override def move(map: mutable.MutableList[Grid], src: Int Tuple2 Int, dest: Int Tuple2 Int): Unit = {
    val sGrid = SearchServiceImpl.searchGridByPos(map, src._1, src._2)
    val dGrid = SearchServiceImpl.searchGridByPos(map, dest._1, dest._2)

    val attacker = sGrid match {
      case Some(a) ⇒ a.gameEntity
      case None ⇒ return
    }
    val defender = dGrid match {
      case Some(a) ⇒ a.gameEntity
      case None ⇒ return
    }

    val srcGrid = sGrid.get
    val destGrid = dGrid.get

    attacker match {
      case attackable: Attackable if defender.isInstanceOf[Targetable] ⇒
        defender.currentHP = defender.currentHP - attackable.damage
        if (defender.currentHP <= 0) {
          destGrid.gameEntity = srcGrid.gameEntity
          srcGrid.gameEntity = null
        }
      case _ ⇒
        destGrid.gameEntity = srcGrid.gameEntity
        srcGrid.gameEntity = null
    }

  }

  /**
    * Build tower
    *
    * @param player
    * @param building
    * @param map
    * @param dest
    */
  override def build(player: ActorRef, building: Building, buildings: mutable.MutableList[Building], map: mutable.MutableList[Grid], dest: Int Tuple2 Int): Unit = {
    implicit val timeout: Timeout = 5 seconds
    val dGrid = SearchServiceImpl.searchGridByPos(map, dest._1, dest._2)
    val destGrid = dGrid match {
      case Some(d) ⇒ d
      case None ⇒ new Grid
    }

    val playerIdFut: Future[Int] = (player ? Player.GetId).mapTo[Int]
    val playerId: Int = Await.result(playerIdFut, 3 seconds)

    if (destGrid.owner == playerId && destGrid.gameEntity == null) {
      destGrid.gameEntity = building

      buildings += building
    }
  }
}
