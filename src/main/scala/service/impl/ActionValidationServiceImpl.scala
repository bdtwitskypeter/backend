package service.impl

import model.game.{Grid, Player, GameMap}
import service.ActionValidationService

/**
  * Created by Trung on 3/22/2018.
  */
object ActionValidationServiceImpl extends ActionValidationService{

    /**
      * Check if location is a point within the map
      *
      * @param map
      * @param grid
      * @return
      */
    override def isValidLocation(map: GameMap, grid: Grid): Boolean = {
        val posX = grid.positionX
        val posY = grid.positionY

        posX < map.numberOfColumns && posY < map.numberOfColumns && posX > 0 && posY > 0
    }

    /**
      * Check is build request is valid
      *
      * @param player
      * @param map
      * @param grid
      * @return
      */
    override def isValidBuild(player: Player, map: GameMap, grid: Grid): Boolean = {
        if (!isValidLocation(map, grid)) false

        ???
    }
}
