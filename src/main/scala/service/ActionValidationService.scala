package service

import model.game.{Grid, Player, GameMap}
/**
  * Created by Trung on 3/22/2018.
  */
trait ActionValidationService {

    /**
      * Check if location is a point within the map
      * @param map
      * @param grid
      * @return
      */
    def isValidLocation(map: GameMap, grid: Grid): Boolean

    /**
      * Check is build request is valid
      * @param player
      * @param map
      * @param grid
      * @return
      */
    def isValidBuild(player: Player, map: GameMap, grid: Grid): Boolean
}
