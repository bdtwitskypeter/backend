package service

import model.game.GameMap

/**
  * Created by Trung on 3/22/2018.
  */
trait TerritoryService {

    /**
      * Update territory
      * @param map
      */
    def updateTerritory(map: GameMap): Unit
}
