package service

import model.game.{GameMap, Grid}

import scala.collection.mutable
/**
  * Created by Trung on 4/3/2018.
  */
trait SearchService {

    /**
      * Search for grid given position
      * @param posX
      * @param posY
      */
    def searchGridByPos(map: mutable.MutableList[Grid], posX: Int, posY: Int): Option[Grid]

    /**
      * Search map by map id
      * @param id
      */
    def searchMapById(id: Int): Option[GameMap]
}
