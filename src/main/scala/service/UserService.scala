package service

import model.online.User

/**
  * Created by Trung on 4/4/2018.
  */
trait UserService {

    /**
      * User join room
      * @param roomId
      */
    def joinRoom(user: User, roomId: Int): Unit

    /**
      * Find user object by id
      * @param userId
      */
    def findUserById(userId: Int): Option[User]
}
