package service

import akka.actor.{Actor, ActorLogging, Cancellable, Props}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._


object Clock {
  def props() = Props(new Clock())
  case object Tick // tick by 1 and start scheduled ticks
  case object GetTick // returns TickResponse
  case object Cancel // stop scheduled tick
  case class TickResponse(tick: Long)  // response containing current tick
}


/*
 * service.Clock actor
 */
class Clock() extends Actor with ActorLogging {

  import Clock._
  def handlerWithState(time: Long, schedule: Option[Cancellable] = None): Receive = {
    case Tick ⇒
      schedule match {
        case Some(_) ⇒ context become handlerWithState(time + 1, schedule)
        case None ⇒ context become handlerWithState(
          time,
          Some(context.system.scheduler.schedule(0 milliseconds, 5 seconds, context.self, Tick)))
      }
    case GetTick => sender ! TickResponse(time)
    case Cancel => context become handlerWithState(time)
  }

  override def receive: Receive = handlerWithState(0)
}
