package service

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import spray.json.{DefaultJsonProtocol, RootJsonFormat}

// json models
final case class UserCreation(user_name: String)

final case class RoomCreation(player_id: Int)
final case class RoomStart(room_id: Int)

final case class PlayerExit(player_id: Int)
final case class PlayerJoin(room_id: Int, player_id: Int)

final case class MoveAction(posX_origin: Int, posY_origin: Int, posX_dest: Int, posY_dest: Int, game_id: Int)
final case class BuildAction(posX_dest: Int, posY_dest: Int, game_id: Int, player_id: Int)

final case class GameUpdate(map_id: Int, game_status: String)


trait JsonSupport extends SprayJsonSupport with DefaultJsonProtocol {
  implicit val userCreateFormat: RootJsonFormat[UserCreation] = jsonFormat1(UserCreation)
  implicit val roomCreateFormat: RootJsonFormat[RoomCreation] = jsonFormat1(RoomCreation)
  implicit val roomStartFormat: RootJsonFormat[RoomStart] = jsonFormat1(RoomStart)
  implicit val playerExitFormat: RootJsonFormat[PlayerExit] = jsonFormat1(PlayerExit)
  implicit val playerJoinFormat: RootJsonFormat[PlayerJoin] = jsonFormat2(PlayerJoin)
  implicit val actionMoveFormat: RootJsonFormat[MoveAction] = jsonFormat5(MoveAction)
  implicit val actionBuildFormat: RootJsonFormat[BuildAction] = jsonFormat4(BuildAction)
  implicit val gameUpdateFormat: RootJsonFormat[GameUpdate] = jsonFormat2(GameUpdate)
}