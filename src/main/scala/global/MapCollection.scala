package global

import model.game.GameMap

import scala.collection.mutable.Map

/**
  * Created by Trung on 4/3/2018.
  */
object MapCollection {

    private var _map = Map.empty[Int, GameMap]

    def map = _map

    def map_= (value: Map[Int, GameMap]): Unit = _map = value
}
