package global

import model.online.User

import scala.collection.mutable

/**
  * Created by Trung on 4/4/2018.
  */
object UserCollection {

    private var _users = new scala.collection.mutable.HashMap[Int, User]()

    def users = _users

    def users_= (value: mutable.HashMap[Int, User]) = _users = value
}
