package global

/**
  * Created by Trung on 4/3/2018.
  */
object IdCollection {

    private var _userId = 1

    private var _playerId = 1

    private var _mapId = 1

    private var _gameEntityId = 1

    def userId = {
        _userId += 1
        _userId
    }

    def playerId = {
        _playerId += 1
        _playerId
    }

    def mapId = {
        _mapId += 1
        _mapId
    }

    def gameEntityId = {
        _gameEntityId += 1
        _gameEntityId
    }

    def userId_= (value: Int): Unit = _userId = value

    def playerId_= (value: Int): Unit = _playerId = value

    def mapId_= (value: Int): Unit = _mapId = value

    def gameEntityId_= (value: Int): Unit = _gameEntityId = value
}
