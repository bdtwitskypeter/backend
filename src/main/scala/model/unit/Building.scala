package model.unit

import model.game.{GameEntity, Player}
import model.interface.Targetable

/**
  * Created by Trung on 3/22/2018.
  */
class Building extends GameEntity with Targetable{

    private var _territoryRange = 0

    var timeStamp = 0

    def territoryRange = _territoryRange

    def territoryRange_= (value: Int): Unit = _territoryRange = value

    /**
      * Reaction when is attacked
      *
      * @param attacker
      */
    override def isAttacked(attacker: Character): Unit = ???
}
