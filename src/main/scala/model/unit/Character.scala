package model.unit

import model.game.{GameEntity, Grid}
import model.interface.{Attackable, Moveable, Targetable}
import service.impl.{GameplayServiceImpl, SearchServiceImpl}

/**
  * Created by Trung on 3/22/2018.
  */
class Character extends GameEntity with Attackable with Moveable with Targetable{

    /**
      * Move to location given grid
      * @param currentPos
      * @param dest
      */
    override def move(currentPos: Grid, dest: Grid): Unit = {
        val map = SearchServiceImpl.searchMapById(this.mapId)
        val srcGrid = currentPos

        map match {
            case Some(nonNullMap) =>
                GameplayServiceImpl.move(nonNullMap.gridList,
                  (srcGrid.positionX, srcGrid.positionY),
                  (dest.positionX, dest.positionY)
                )
            case None =>
                throw new RuntimeException("Map doesn't exist")
        }
    }

    /**
      * Reaction when is attacked
      * @param attacker
      */
    override def isAttacked(attacker: Character): Unit = {
//        val destGrid = attacker.po
//        val srcGrid = this.position

        val map = SearchServiceImpl.searchMapById(this.mapId)

        map match {
            case Some(nonNullMap) =>
                GameplayServiceImpl.move(nonNullMap.gridList,
                                            Tuple2 (_positionX, _positionY),
                                            Tuple2 (attacker._positionX, attacker._positionY) )
            case None =>
                throw new RuntimeException("Map doesn't exist")
        }
    }
}
