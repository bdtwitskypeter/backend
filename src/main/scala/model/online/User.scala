package model.online

/**
  * Created by Trung on 4/2/2018.
  */
class User {

    private var _id = 0

    private var _username = ""

    private var _password = ""

    private var _name = ""

    private var _roomId = 0

    private var _inGame = false

    private var _inRoom = false

    def id = _id

    def id_= (value: Int): Unit = _id = value

    def username = _username

    def username_= (value: String): Unit = _username = value

    def password = _password

    def password_= (value: String): Unit = _password = value

    def name = _name

    def name_= (value: String): Unit = _name = value

    def roomId = _roomId

    def roomId_= (value: Int): Unit = _roomId = value

    def inGame = _inGame

    def inGame_= (value: Boolean): Unit = _inGame = value

    def inRoom = _inRoom

    def inRoom_= (value: Boolean): Unit = _inRoom = value
}
