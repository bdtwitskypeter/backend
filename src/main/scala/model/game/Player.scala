package model.game

import akka.actor.{Actor, ActorLogging, ActorSystem, Props}
import akka.util.Timeout
import akka.pattern.ask
import global.IdCollection
import model.game.Player._
import model.online.User
import service.Clock
import service.Clock.GetTick

import scala.concurrent.Await
import scala.concurrent.duration._

/**
  * Created by Trung on 3/22/2018.
  */
object Player {
  def props(user: User, mapId: Int) = Props{
    val player = new Player()
    player.id = IdCollection.playerId
    player.actionPoint = 20
    player.gold = 100
    player.prevTick = 0L
    player.mapId = mapId
    player
  }

    case object GetId

    case object GetActionPoint

    case object GetGold

    case object AssetIncrement

    case object UseAp
}

class Player() extends Actor with ActorLogging{

    private var _id = 0

    private var _mapId = 0

    private var _actionPoint = 0

    private var _gold = 0

    private var _prevTick = 0L

    def id = _id

    def mapId = _mapId

    def actionPoint = _actionPoint

    def gold = _gold

    def prevTick = _prevTick

    def id_= (value: Int): Unit = _id = value

    def mapId_= (value: Int): Unit = _mapId = value

    def actionPoint_= (value: Int): Unit = _actionPoint = value

    def gold_= (value: Int): Unit = _gold = value

    def prevTick_= (value: Long): Unit = _prevTick = value

    /**
      * Handle message on receive
      * @param apUsed
      * @return
      */
    def handler(apUsed: Option[Int] = None): Receive = {
        case GetId => sender ! this.id
        case GetActionPoint => sender ! this.actionPoint
        case GetGold => sender ! this.gold
        case AssetIncrement =>
            val system = ActorSystem("ClockSystem")
            val clock = system.actorOf(Props[Clock], "Clock")

            implicit val timeOut = Timeout(5 seconds)
            val future = clock ? GetTick
            val result = Await.result(future, timeOut.duration).asInstanceOf[Long]

            if (result > this.prevTick) {
                this.actionPoint = this.actionPoint + 5
                this.gold = this.gold + 50
            }
        case UseAp =>
            apUsed match {
                case Some(ap) =>
                    this.actionPoint = this.actionPoint - ap
                    sender ! this.actionPoint
            }
    }

    override def receive: Receive = handler()
}