package model.game

import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import factory.BuildingFactory
import global.{IdCollection, MapCollection}
import model.game.GameMap._
import model.unit.Building
import service.impl.GameplayServiceImpl
import service.{BuildAction, MoveAction}

import scala.collection.mutable

/**
  * Created by Trung on 3/22/2018.
  */
object GameMap {
  def props(id: Int) = Props {
    val gameMap = new GameMap
    gameMap.id = id
    gameMap.numberOfColumns = 20
    gameMap.numberOfRows = 20
    gameMap.isReady = false
    gameMap.gridList = new mutable.MutableList[Grid]
    gameMap.buildingList = new mutable.MutableList[Building]

    MapCollection.map += gameMap.id -> gameMap
    gameMap
  }

  case object GetId

  case object GetNumberOfRows

  case object GetNumberOfColumns

  case object GetGridList

  case object GetBuildingList

  case class Move(moveAction: MoveAction)
  case class Build(player: ActorRef, buildAction: BuildAction)
  case object Status
}

class GameMap() extends Actor with ActorLogging{

    private var _id = 0

    private var _numberOfRows = 0

    private var _numberOfColumn = 0

    private var _isReady = false

    private var _gridList = mutable.MutableList(new Grid)

    private var _buildingList = mutable.MutableList(new Building)

    def id = _id

    def numberOfRows = _numberOfRows

    def numberOfColumns = _numberOfColumn

    def isReady = _isReady

    def gridList: mutable.MutableList[Grid] = _gridList

    def buildingList: mutable.MutableList[Building] = _buildingList

    def id_= (value: Int) = _id = value

    def numberOfRows_= (value: Int) = _numberOfRows = value

    def numberOfColumns_= (value: Int) = _numberOfColumn = value

    def isReady_= (value: Boolean) = _isReady = value

    def gridList_=(gridList: mutable.MutableList[Grid]) = _gridList = gridList

    def buildingList_= (buildingList: mutable.MutableList[Building]) = _buildingList = buildingList

    /**
      * Handle message on receive
      * @return
      */
    def handler(): Receive = {
        case GetId => sender ! this.id
        case GetNumberOfRows => sender ! this.numberOfRows
        case GetNumberOfColumns => sender ! this.numberOfColumns
        case GetGridList => sender ! this.gridList
        case GetBuildingList => sender ! this.buildingList
        case Move(a) ⇒ GameplayServiceImpl.move( gridList, (a.posX_origin, a.posY_origin), (a.posX_dest, a.posY_dest))
        case Build(p, a) ⇒
          val b = BuildingFactory.makeBuilding(p, _gridList, _buildingList, a.posX_dest, a.posY_dest)
          b match {
            case Some(bld) ⇒
              GameplayServiceImpl.build(p, bld, _buildingList, _gridList, (a.posX_dest,a.posY_dest))
            case _ ⇒
          }
    }

    override def receive: Receive = handler()
}