package model.game

import akka.actor.ActorRef

/**
  * Created by Trung on 3/22/2018.
  */
class Grid() {

    private var _positionX = 0

    private var _positionY = 0

    private var _gameEntity = new GameEntity()

    def owner = 0

    def positionX = _positionX

    def positionY = _positionY

    def gameEntity = _gameEntity

    def positionX_= (value: Int): Unit = _positionX = value

    def positionY_= (value: Int): Unit = _positionY = value

    def gameEntity_= (gameEntity: GameEntity): Unit = _gameEntity = gameEntity

    def owner_=(owner_id: Int): Unit = owner = owner_id
}
