package model.game

import akka.actor.ActorRef

/**
  * Created by Trung on 3/22/2018.
  */
class GameEntity {

    var _positionX = 0

    var _positionY = 0

    private var _id = 0

    private var _mapId = 0

    private var _requiredActionPoint = 0

    private var _currentHP = 0

    private var _maxHP = 0

    private var _visionRange = 0

    private var _owner_id = 0

    def id = _id

    var owner = _owner_id

    def requiredActionPoint = _requiredActionPoint

    def currentHP = _currentHP

    def maxHP = _maxHP

    def visionRange = _visionRange

    def id_= (value: Int): Unit = _id = value

    def requiredActionPoint_= (value: Int): Unit = _requiredActionPoint = value

    def currentHP_= (value: Int): Unit = _currentHP = value

    def maxHP_= (value: Int): Unit = _maxHP = value

    def visionRange_= (value: Int): Unit = _visionRange = value

    def mapId = _mapId

    def mapId_= (value: Int): Unit = _mapId = mapId
}
