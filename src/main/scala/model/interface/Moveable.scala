package model.interface

import model.game.Grid

/**
  * Created by Trung on 3/22/2018.
  */
trait Moveable {

    /**
      * Move to location given grid
      * @param currentPos
      * @param dest
      */
    def move(currentPos: Grid, dest: Grid)
}
