package model.interface

import model.unit.Character
/**
  * Created by Trung on 3/22/2018.
  */
trait Targetable {

    var _isAttacked = false

    def isAttacked = _isAttacked

    def isAttacked_= (value: Boolean): Unit = _isAttacked = value

    /**
      * Reaction when is attacked
      * @param attacker
      */
    def isAttacked(attacker: Character)
}
