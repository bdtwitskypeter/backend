package model.interface

import model.game.Grid

/**
  * Created by Trung on 3/22/2018.
  */
trait Attackable extends Moveable{

    var _attacking = false

    var _damage = 0

    def attacking = _attacking

    def attacking_= (value: Boolean): Unit = _attacking = value

    def damage = _damage

    def damage_= (value: Int): Unit = _damage = value
}
