package model.api

import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import akka.pattern.ask
import akka.util.Timeout
import factory.{MapFactory, PlayerFactory}
import model.game.GameMap
import model.online.User
import service.{BuildAction, MoveAction}

import scala.concurrent.duration._
import scala.concurrent.{Await, Future}

object Room {
  var id: Int = 0
  def getID(): Int = {
    id += 1
    id
  }
  def props() = Props{new Room()}
  case class join(user: User)
  case class exit()
  case class start()
  case class move(moveAction: MoveAction)
  case class build(buildAction: BuildAction)
  case object status
  case class statusResponse(msg: String)

}

class Room() extends Actor with ActorLogging{
  implicit val timeout: Timeout = 5 seconds

  log.info(s"New model.api.Room ${self.path.toString}")
  import Room._

  def baseHandler(): Receive = {
    case join(user) ⇒ {
      log.info(s"User connected: ${user.name}")
      context become singleUserHandler(user)
    }
  }

  def singleUserHandler(user: User): Receive = {
    case join(user2) ⇒ {
      log.info(s"User connected: ${user.name} and ${user2.name}")
      context become lobbyHandler((user, user2))
    }
  }

  def lobbyHandler(users: (User, User)): Receive = {
    case start() ⇒ {
      log.info("model.api.Game started")
      val map: ActorRef = MapFactory.createMap(context.system)
      val mapIdFut: Future[Int] = (map ? GameMap.GetId).mapTo[Int]
      val mapId: Int = Await.result(mapIdFut, 3 seconds)
      val player1: ActorRef = PlayerFactory.createPlayer(context.system, users._1, mapId)
      val player2: ActorRef = PlayerFactory.createPlayer(context.system, users._2, mapId)
      context become gameHandler( (player1, player2), map)
    }
  }

  def gameHandler(players: (ActorRef, ActorRef), map: ActorRef): Receive = {
    case build(build_action) ⇒ map ! GameMap.Build(players._1, build_action)
    case move(move_action) ⇒ map ! GameMap.Move(move_action)
    case status ⇒ sender ! Room.statusResponse("fine")
  }

  override def receive: Receive = baseHandler()
}

