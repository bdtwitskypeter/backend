package model.api

import akka.actor.{Actor, ActorLogging, ActorRef, Props}


object Game {
  def props(player: ActorRef) = Props(new Game(player))
  case class Action()
}

class Game(host: ActorRef) extends Actor with ActorLogging{
  import Game._
  override def receive: Receive = {
    case Action() ⇒
  }
}

