name := "Final Project"

version := "1.0"

scalaVersion := "2.12.1"

libraryDependencies ++= Seq(
    "com.typesafe.akka" %% "akka-actor" % "2.5.11",
    "com.typesafe.akka" %% "akka-testkit" % "2.5.11" % Test,
    "com.typesafe.akka" %% "akka-stream" % "2.5.11",
    "com.typesafe.akka" %% "akka-stream-testkit" % "2.5.11" % Test,
    "com.typesafe.akka" %% "akka-http" % "10.0.11",
    "com.typesafe.akka" %% "akka-http-testkit" % "10.0.11" % Test,
    "com.typesafe.akka" %% "akka-http-spray-json" % "10.1.0"
)